#!/bin/bash

# Enter
zpool import -N -R /mnt rpool
zfs load-key -a
zfs mount rpool/ROOT
zfs mount -a
mount /dev/vda1 /mnt/boot

# Exit
zfs umount -a
umount /mnt/boot
zfs umount rpool/ROOT
zpool export -a