#!/bin/bash

DISK=/dev/disk/by-id/virtio-SSD-01 && \
sgdisk  --zap-all              $DISK && \
sgdisk  -n1:24K:+512M -t1:EF00 $DISK && \
sgdisk  -n2:0:+1G     -t2:BF01 $DISK && \
sgdisk  -n3:0:0       -t3:BF01 $DISK

# ESP
mkfs.fat -F32 ${DISK}-part1

zpool create -o ashift=12 -d \
    -o feature@async_destroy=enabled \
    -o feature@bookmarks=enabled \
    -o feature@embedded_data=enabled \
    -o feature@empty_bpobj=enabled \
    -o feature@enabled_txg=enabled \
    -o feature@extensible_dataset=enabled \
    -o feature@filesystem_limits=enabled \
    -o feature@hole_birth=enabled \
    -o feature@large_blocks=enabled \
    -o feature@lz4_compress=enabled \
    -o feature@spacemap_histogram=enabled \
    -o feature@userobj_accounting=enabled \
    -o feature@zpool_checkpoint=enabled \
    -o feature@spacemap_v2=enabled \
    -o feature@project_quota=enabled \
    -o feature@resilver_defer=enabled \
    -o feature@allocation_classes=enabled \
    -O acltype=posixacl -O canmount=off -O compression=lz4 -O devices=off \
    -O normalization=formD -O relatime=on -O xattr=sa \
    -O mountpoint=/ -R /mnt bpool ${DISK}-part2

# rpoolpass
zpool create -o ashift=12 \
    -O acltype=posixacl -O canmount=off -O compression=lz4 \
    -O dnodesize=auto -O normalization=formD -O relatime=on -O xattr=sa \
    -O encryption=aes-256-gcm -O keylocation=prompt -O keyformat=passphrase \
    -O mountpoint=/ -R /mnt rpool ${DISK}-part3

###
zfs create -o canmount=off -o mountpoint=none rpool/ROOT && \
zfs create -o canmount=off -o mountpoint=none bpool/BOOT

zfs create -o canmount=noauto -o mountpoint=/ rpool/ROOT/arch && \
zfs mount rpool/ROOT/arch && \
zfs create -o canmount=noauto -o mountpoint=/boot bpool/BOOT/arch && \
zfs mount bpool/BOOT/arch
###

zfs create -o canmount=noauto -o mountpoint=/ rpool/ROOT && \
zfs mount rpool/ROOT && \
zfs create -o canmount=noauto -o mountpoint=/boot bpool/BOOT && \
zfs mount bpool/BOOT

zfs create                                 rpool/home && \
zfs create                                 rpool/home/andrei && \
zfs create -o mountpoint=/root             rpool/home/root && \
zfs create -o canmount=off                 rpool/var && \
zfs create -o canmount=off                 rpool/var/lib && \
zfs create -o com.sun:auto-snapshot=false  rpool/var/lib/docker && \
zfs create                                 rpool/var/www && \
zfs create                                 rpool/var/log && \
zfs create -o com.sun:auto-snapshot=false  rpool/var/cache && \
zfs create -o com.sun:auto-snapshot=false  rpool/var/tmp && \
zfs create                                 rpool/srv && \
chmod 1777 /mnt/var/tmp

# Install arch
pacstrap /mnt base linux linux-firmware linux-headers archzfs-linux vim man-db man-pages texinfo grub efibootmgr openssh mkinitcpio-netconf mkinitcpio-dropbear
mount ${DISK}-part1 /mnt/efi
# genfstab -U /mnt >> /mnt/etc/fstab

zfs set devices=off rpool

echo archVM > /mnt/etc/hostname
# Add 127.0.1.1       archVM
vim /mnt/etc/hosts

arch-chroot /mnt
DISK=/dev/disk/by-id/virtio-SSD-01
### Inside chroot ###
ln -sf /usr/share/zoneinfo/Europe/Oslo /etc/localtime
hwclock --systohc
vim /etc/locale.gen
# Uncomment en_US.UTF-8 UTF-8
echo "LANG=en_US.UTF-8" > /etc/locale.conf

cat <<EOF > /etc/hosts
127.0.0.1   localhost
::1     localhost
127.0.1.1   archVM.local archvm
EOF

# Network setup
systemctl enable systemd-networkd.service systemd-resolved.service
cat <<EOF > /etc/systemd/network/20-wired.network
[Match]
Name=enp1s0

[Network]
DHCP=yes
EOF

# initramfs
mkinitcpio -P
vim /etc/mkinitcpio.conf
# HOOKS=(base udev autodetect modconf block keyboard zfs filesystems)

# Add archzfs repo
vim /etc/pacman.conf
[archzfs]
Server = http://archzfs.com/$repo/x86_64
SigLevel = Optional TrustAll

# Grub
echo PARTUUID=$(blkid -s PARTUUID -o value ${DISK}-part1) \
    /efi vfat nofail,x-systemd.device-timeout=1 0 1 >> /etc/fstab

# Set root password
passwd

## NOT NEEDED??
cat <<EOF > /etc/systemd/system/zfs-import-bpool.service
[Unit]
DefaultDependencies=no
Before=zfs-import-scan.service
Before=zfs-import-cache.service

[Service]
Type=oneshot
RemainAfterExit=yes
ExecStart=/sbin/zpool import -N -o cachefile=none bpool

[Install]
WantedBy=zfs-import.target
EOF

systemctl enable zfs-import-bpool.service
###

vim /etc/default/grub
# Set: GRUB_CMDLINE_LINUX="root=ZFS=rpool/ROOT/arch"
# Remove quiet from: GRUB_CMDLINE_LINUX_DEFAULT
# Uncomment: GRUB_TERMINAL=console

ZPOOL_VDEV_NAME_PATH=1 grub-install --target=x86_64-efi --efi-directory=/efi \
--bootloader-id=arch --recheck --no-floppy

ZPOOL_VDEV_NAME_PATH=1 grub-mkconfig -o /boot/grub/grub.cfg

umount /efi && \
zfs set mountpoint=legacy bpool/BOOT && \
echo bpool/BOOT /boot zfs \
    nodev,relatime,x-systemd.requires=zfs-import-bpool.service 0 0 >> /etc/fstab && \
zed -F &

cat /etc/zfs/zfs-list.cache/rpool
# If empty
zpool set cachefile=/etc/zfs/zpool.cache rpool && \
zfs set canmount=noauto rpool/ROOT && \
cat /etc/zfs/zpool.cache

sed -Ei "s|/mnt/?|/|" /etc/zfs/zpool.cache

zfs snapshot bpool/BOOT@install && \
zfs snapshot rpool/ROOT@install

# Out of chroot
mount | grep -v zfs | tac | awk '/\/mnt/ {print $3}' | xargs -i{} umount -lf {} && \
zpool export -a

# After boot
zpool set cachefile=/etc/zfs/zpool.cache rpool
zpool set cachefile=/etc/zfs/zpool.cache bpool
systemctl enable zfs.target
systemctl enable zfs-import-cache
systemctl enable zfs-mount
systemctl enable zfs-import.target

### Dropbear SSH
vim /etc/dropbear/root_key
# Add client public key
vim /etc/default/grub
# Add ip=:::::eth0:dhcp param
vim /etc/mkinitcpio.conf
# Add netconf dropbear zfsencryptssh BEFORE zfs
# Change dropbear port
cp /usr/lib/initcpio/hooks/dropbear /etc/initcpio/hooks/dropbear
mkinitcpio -P

USERNAME=andrei && \
zfs create rpool/home/$USERNAME && \
adduser $USERNAME && \
cp -a /etc/skel/. /home/$USERNAME && \
chown -R $USERNAME:$USERNAME /home/$USERNAME

# Swap zvol
zfs create -V 4G -b $(getconf PAGESIZE) -o compression=zle \
    -o logbias=throughput -o sync=always \
    -o primarycache=metadata -o secondarycache=none \
    -o com.sun:auto-snapshot=false rpool/swap && \
mkswap -f /dev/zvol/rpool/swap && \
echo /dev/zvol/rpool/swap none swap discard 0 0 >> /etc/fstab && \
echo RESUME=none > /etc/initramfs-tools/conf.d/resume && \
swapon -av

# Disable log compression
for file in /etc/logrotate.d/* ; do
    if grep -Eq "(^|[^#y])compress" "$file" ; then
        sed -i -r "s/(^|[^#y])(compress)/\1#\2/" "$file"
    fi
done

### Live CD rescue
zpool export -a
zpool import -N -R /mnt rpool
zpool import -N -R /mnt bpool
zfs load-key -a
zfs mount rpool/ROOT
zfs mount -a