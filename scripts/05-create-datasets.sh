#!/bin/sh

set -e

echo "Create root dataset"
zfs create -o canmount=noauto -o mountpoint=/ rpool/ROOT
zfs mount rpool/ROOT

echo "Create child datasets"
zfs create                                 rpool/home
zfs create                                 rpool/home/andrei
zfs create                                 rpool/home/git
zfs create -o mountpoint=/root             rpool/home/root
zfs create                                 rpool/opt
zfs create                                 rpool/srv
zfs create                                 rpool/srv/containers
zfs create                                 rpool/srv/general
zfs create                                 rpool/srv/web
zfs create                                 rpool/var
zfs create -o com.sun:auto-snapshot=false  rpool/var/cache
zfs create                                 rpool/var/lib
zfs create -o com.sun:auto-snapshot=false  rpool/var/lib/containers
zfs create -o com.sun:auto-snapshot=false  rpool/var/lib/docker
zfs create -o recordsize=8K \
           -o primarycache=metadata \
           -o logbias=throughput           rpool/var/lib/postgres
zfs create -o com.sun:auto-snapshot=false  rpool/var/log
zfs create -o com.sun:auto-snapshot=false  rpool/var/tmp
zfs create                                 rpool/var/www

chmod 1777 /mnt/var/tmp
