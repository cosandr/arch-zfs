#!/bin/bash

set -o pipefail -e

echo "mkswap"
mkswap $DISK-part3
echo "Swap to fstab"
cat <<EOF >> /etc/fstab
# Swap partition on $DISK
UUID=$(lsblk -dno UUID $DISK-part3) none swap defaults 0 0
EOF

echo "Installing systemd-boot to /boot"
bootctl --path=/boot install

kernel_opts="root=ZFS=rpool/ROOT rw ip=dhcp nmi_watchdog=0 apparmor=1 security=apparmor"
echo "arch.conf"
cat <<EOF > /boot/loader/entries/arch.conf
title   Arch Linux
linux   /vmlinuz-linux
initrd  /intel-ucode.img
initrd  /initramfs-linux.img
options $kernel_opts
EOF

echo "arch-lts.conf"
cat <<EOF > /boot/loader/entries/arch-lts.conf
title   Arch Linux (LTS)
linux   /vmlinuz-linux-lts
initrd  /intel-ucode.img
initrd  /initramfs-linux-lts.img
options $kernel_opts
EOF

echo "loader.conf, default to LTS kernel"
cat <<EOF > /boot/loader/loader.conf
default arch-lts
timeout 2
console-mode max
editor yes
EOF

echo "Fix ZFS mounting on boot"
mkdir /etc/zfs/zfs-list.cache
touch /etc/zfs/zfs-list.cache/rpool
zpool set cachefile=/etc/zfs/zfs-list.cache/rpool rpool
ln -s "$(find /usr/lib -maxdepth 1 -type d -name "zfs-*" -print)/zfs/zed.d/history_event-zfs-list-cacher.sh" "/etc/zfs/zed.d"
zed -F &
sleep 2
zfs set canmount=on rpool/var
sleep 2
sed -Ei "s|/mnt/?|/|" /etc/zfs/zfs-list.cache/rpool
cat /etc/zfs/zfs-list.cache/rpool
read -rp "CTRL-C if table isn't visible"

echo "Snapshot initial install"
zfs snapshot -r rpool@install

echo "Check fstab, keep legacy, rpool/ROOT and ESP mounts"
sleep 2
vim /etc/fstab

cat <<EOF
Exit chroot, unmount and export all pools
zfs umount -a
umount /mnt/boot
zfs umount rpool/ROOT
zpool export -a
EOF
