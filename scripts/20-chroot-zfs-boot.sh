#!/bin/bash

echo "!!! OUTDATED !!!"

set -o pipefail -o noclobber -e

echo "mkswap"
mkswap $DISK-part4
echo "Swap to fstab"
cat <<EOF >> /etc/fstab

# Swap partition on $DISK
UUID=$(lsblk -dno UUID $DISK-part4) none swap defaults 0 0
EOF

echo "Grub config"
sed -i 's/^GRUB_CMDLINE_LINUX=.*/GRUB_CMDLINE_LINUX=\"root=ZFS=rpool\/ROOT\"/' /etc/default/grub
sed -i '0,/quiet/s/quiet/ip=dhcp nmi_watchdog=0 apparmor=1 security=apparmor/' /etc/default/grub

echo "Install grub to /efi"
ZPOOL_VDEV_NAME_PATH=1 grub-install --target=x86_64-efi --efi-directory=/efi \
--bootloader-id=arch --recheck --no-floppy

echo "Create /boot/grub/grub.cfg"
ZPOOL_VDEV_NAME_PATH=1 grub-mkconfig -o /boot/grub/grub.cfg

echo "Fix ZFS mounting on boot"
mkdir /etc/zfs/zfs-list.cache
touch /etc/zfs/zfs-list.cache/rpool
touch /etc/zfs/zfs-list.cache/bpool
zpool set cachefile=/etc/zfs/zfs-list.cache/rpool rpool
zpool set cachefile=/etc/zfs/zfs-list.cache/bpool bpool
ln -s "/usr/lib/zfs-0.8.2/zfs/zed.d/history_event-zfs-list-cacher.sh" "/etc/zfs/zed.d"
zed -F &
zfs set canmount=on rpool/var
zfs set mountpoint=legacy bpool/BOOT
sed -Ei "s|/mnt/?|/|" /etc/zfs/zfs-list.cache/rpool
sed -Ei "s|/mnt/?|/|" /etc/zfs/zfs-list.cache/bpool
cat /etc/zfs/zfs-list.cache/rpool
cat /etc/zfs/zfs-list.cache/bpool
read -r "CTRL-C if empty"

echo "Snapshot initial install"
zfs snapshot bpool/BOOT@install
zfs snapshot rpool/ROOT@install

cat <<EOF
CHECK FSTAB!!!
Keep legacy, rpool/ROOT and ESP mounts

Exit chroot, unmount and export all pools
zfs umount -a
umount /mnt/efi
zfs umount rpool/ROOT
zfs umount bpool/BOOT
zpool export -a
EOF
