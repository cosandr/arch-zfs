#!/bin/bash

set -o pipefail -o noclobber -o nounset -e

# After boot
zpool set cachefile=/etc/zfs/zpool.cache rpool
zpool set cachefile=/etc/zfs/zpool.cache bpool
systemctl enable zfs.target
systemctl enable zfs-import-cache
systemctl enable zfs-mount
systemctl enable zfs-import.target

### Dropbear SSH
vim /etc/dropbear/root_key
# Add client public key
vim /etc/default/grub
# Add ip=:::::eth0:dhcp param
vim /etc/mkinitcpio.conf
# Add netconf dropbear zfsencryptssh BEFORE zfs
# Change dropbear port
cp /usr/lib/initcpio/hooks/dropbear /etc/initcpio/hooks/dropbear
mkinitcpio -P

USERNAME=andrei
zfs create rpool/home/$USERNAME
adduser $USERNAME
cp -a /etc/skel/. /home/$USERNAME
chown -R $USERNAME:$USERNAME /home/$USERNAME

# Swap zvol
zfs create -V 4G -b $(getconf PAGESIZE) -o compression=zle \
    -o logbias=throughput -o sync=always \
    -o primarycache=metadata -o secondarycache=none \
    -o com.sun:auto-snapshot=false rpool/swap
mkswap -f /dev/zvol/rpool/swap
echo /dev/zvol/rpool/swap none swap discard 0 0 >> /etc/fstab
echo RESUME=none > /etc/initramfs-tools/conf.d/resume
swapon -av

# Disable log compression
for file in /etc/logrotate.d/* ; do
    if grep -Eq "(^|[^#y])compress" "$file" ; then
        sed -i -r "s/(^|[^#y])(compress)/\1#\2/" "$file"
    fi
done

### Live CD rescue
zpool export -a
zpool import -N -R /mnt rpool
zpool import -N -R /mnt bpool
zfs load-key -a
zfs mount rpool/ROOT
zfs mount -a