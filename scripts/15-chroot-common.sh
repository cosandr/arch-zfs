#!/bin/bash

set -o pipefail -o nounset -e

var_user=andrei
var_hostname="archVM"
var_timezone="Europe/Oslo"

echo "Set hostname"
hostnamectl set-hostname "$var_hostname"

ln -sf /usr/share/zoneinfo/"$var_timezone" /etc/localtime
hwclock --systohc

echo "Setting locale"
sed -i '/^#en_US\.UTF-8 UTF-8/s/^#//g' /etc/locale.gen
sed -i '/^#en_GB\.UTF-8 UTF-8/s/^#//g' /etc/locale.gen
sed -i '/^#nb_NO\.UTF-8 UTF-8/s/^#//g' /etc/locale.gen
locale-gen

cat <<EOF > /etc/locale.conf
LANG=en_US.UTF-8
LC_TIME=en_GB.UTF-8
EOF

echo "Set /etc/hosts"
cat <<EOF > /etc/hosts
127.0.0.1   localhost
::1     localhost
EOF

# Network setup
echo "systemd-networkd setup"
systemctl enable systemd-networkd.service systemd-resolved.service
primary_inet=$(route | grep '^default' | grep -m 1 -o '[^ ]*$')
echo "Enable DHCP for primary network adapter: ${primary_inet}"
cat <<EOF > /etc/systemd/network/20-wired.network
[Match]
Name=$primary_inet

[Network]
DHCP=yes
EOF

echo "Some default /etc/profile options"
cat <<EOF > /etc/profile.d/mine.sh
export EDITOR='vim'
export VISUAL='vim'
export PAGER='less'
alias ll='ls -lh'
alias la='ls -lha'
EOF

echo "ZSH /etc/zsh/zprofile"
cat <<EOF >> /etc/zsh/zprofile
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/site-functions
EOF

chsh -s /usr/bin/zsh

echo "Create $var_user user"
useradd -s /usr/bin/zsh -d /home/${var_user} -G wheel ${var_user}
cp -a /etc/skel/. /home/${var_user}
chown -R ${var_user}:${var_user} /home/${var_user}
chmod 700 /home/${var_user}

echo "Allow wheel group access to sudo"
sed -i '0,/^# %wheel ALL=(ALL) ALL/s//%wheel ALL=(ALL) ALL/' /etc/sudoers
# echo "Regenerate SSH host keys"
# ssh-keygen -q -t dsa -m PEM -N '' -f /etc/ssh/ssh_host_dsa_key 2>/dev/null <<< y >/dev/null
# ssh-keygen -q -t rsa -m PEM -N '' -f /etc/ssh/ssh_host_rsa_key 2>/dev/null <<< y >/dev/null
# ssh-keygen -q -t ecdsa -m PEM -N '' -f /etc/ssh/ssh_host_ecdsa_key 2>/dev/null <<< y >/dev/null
# ssh-keygen -q -t ed25519 -m PEM -N '' -f /etc/ssh/ssh_host_ed25519_key 2>/dev/null <<< y >/dev/null

echo "initramfs setup"
sed -i 's/^HOOKS=.*/HOOKS=\(base udev autodetect modconf block keyboard netconf dropbear zfsencryptssh zfs filesystems\)/g' /etc/mkinitcpio.conf

cp /usr/lib/initcpio/hooks/dropbear /etc/initcpio/hooks/dropbear
sed -i '/^\s*\/usr\/sbin\/dropbear/ s/$/ -p 2222/' /etc/initcpio/hooks/dropbear
cp /root/.ssh/authorized_keys /etc/dropbear/root_key

# echo "Generate Dropbear host keys"
# dropbearkey -t rsa -f /etc/dropbear/dropbear_rsa_host_key
# dropbearkey -t ecdsa -f /etc/dropbear/dropbear_ecdsa_host_key

mkinitcpio -P

# Add archzfs repo
echo "Adding archzfs repo"
cat <<EOF >> /etc/pacman.conf

[archzfs]
Server = http://archzfs.com/\$repo/x86_64
EOF
pacman-key -r F75D9D76
pacman-key --lsign-key F75D9D76

echo "Enable verbose output and color"
sed -i '/VerbosePkgLists/s/^#//g' /etc/pacman.conf
sed -i '/Color/s/^#//g' /etc/pacman.conf

echo "Enable ZFS, ssh and apparmor services"
systemctl enable zfs-zed.service zfs.target zfs-import-cache zfs-mount zfs-import.target sshd apparmor auditd
echo "Change default target to multi-user"
systemctl set-default multi-user.target

echo "Enable AppArmor caching"
sed -i '/^#write-cache/s/^#//g' /etc/apparmor/parser.conf

# Set root password
echo "Set root password"
passwd

echo "Set andrei password"
passwd andrei
