#!/bin/bash

echo "!!! OUTDATED !!!"

set -o pipefail -o noclobber -o nounset -e

export DISK=/dev/disk/by-id/virtio-SSD-01

ESP_SIZE="512M"
SWAP_SIZE="$(awk '/MemTotal/ {print $2}' /proc/meminfo)K"
echo "Using $DISK, ESP $ESP_SIZE, swap $SWAP_SIZE"
sleep 2

sgdisk  --zap-all                     $DISK
sgdisk  -n1:24K:+$ESP_SIZE   -t1:EF00 $DISK
sgdisk  -n2:0:+1G            -t2:BF01 $DISK
sgdisk  -n3:0:-"$SWAP_SIZE"  -t3:BF01 $DISK
sgdisk  -n4:0:0              -t4:8200 $DISK

fdisk -l $DISK
sleep 1

# Format ESP
echo "Format ESP"
mkfs.fat -F32 ${DISK}-part1

echo "Create GRUB-compatible ZFS pool"
zpool create -o ashift=12 -d \
    -o feature@async_destroy=enabled \
    -o feature@bookmarks=enabled \
    -o feature@embedded_data=enabled \
    -o feature@empty_bpobj=enabled \
    -o feature@enabled_txg=enabled \
    -o feature@extensible_dataset=enabled \
    -o feature@filesystem_limits=enabled \
    -o feature@hole_birth=enabled \
    -o feature@large_blocks=enabled \
    -o feature@lz4_compress=enabled \
    -o feature@spacemap_histogram=enabled \
    -o feature@userobj_accounting=enabled \
    -o feature@zpool_checkpoint=enabled \
    -o feature@spacemap_v2=enabled \
    -o feature@project_quota=enabled \
    -o feature@resilver_defer=enabled \
    -o feature@allocation_classes=enabled \
    -O acltype=posixacl -O canmount=off -O compression=lz4 -O devices=off \
    -O normalization=formD -O relatime=on -O xattr=sa \
    -O mountpoint=/ -R /mnt bpool ${DISK}-part2

# rpoolpass
echo "Create encrypted root pool"
zpool create -o ashift=12 \
    -O acltype=posixacl -O canmount=off -O compression=lz4 \
    -O dnodesize=auto -O normalization=formD -O relatime=on -O xattr=sa \
    -O encryption=aes-256-gcm -O keylocation=prompt -O keyformat=passphrase \
    -O mountpoint=/ -R /mnt rpool ${DISK}-part3

./05-create-datasets.sh

echo "Create boot dataset"
zfs create -o canmount=noauto -o mountpoint=/boot bpool/BOOT
zfs mount bpool/BOOT

echo "Mount ESP to /efi"
mkdir /mnt/efi
mount ${DISK}-part1 /mnt/efi

# Install arch
echo "Installing Arch"
pacstrap /mnt $(<packages)

echo "Setup fstab, needs manual intervention later"
genfstab -U /mnt >> /mnt/etc/fstab

# zfs set devices=off rpool

echo "Done, ready for chroot"
