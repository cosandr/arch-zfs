#!/bin/bash
# ALL AS ROOT

set -e

if [[ -z $PUB_KEY ]]; then
    echo "Set SSH public key to PUB_KEY env var"
    exit 1
fi

pacman -S archiso

cp -r /usr/share/archiso/configs/releng /root/archlive

cat <<EOF >> /root/archlive/pacman.conf
[archzfs]
Server = http://archzfs.com/\$repo/x86_64
SigLevel = Optional TrustAll
EOF

# Add packages to archiso
cat <<EOF >> /root/archlive/packages.x86_64
archzfs-linux
EOF

cat <<EOF >> /root/archlive/airootfs/root/customize_airootfs.sh
systemctl enable sshd.service
mkdir /root/.ssh
chmod 700 /root/.ssh
echo "$PUB_KEY" > /root/.ssh/authorized_keys
chmod 600 /root/.ssh/authorized_keys
EOF

cd /root/archlive
# in archlive directory
./build.sh -v
