#!/bin/bash

kernel_version="linux-lts-5.4.19-1"
kernel_pkg="${kernel_version}-x86_64.pkg.tar.zst"
# Custom repo for kernel
mkdir -p /root/{customrepo/x86_64,pkg}
cd /root/pkg
wget "https://archive.archlinux.org/packages/l/linux/${kernel_pkg}"

repo-add /root/customrepo/x86_64/customrepo.db.tar.gz /root/pkg/"${kernel_pkg}"

cat <<EOF >> /root/archlive/pacman.conf
[customrepo]
SigLevel = Optional TrustAll
Server = file:///root/customrepo/\$arch
EOF
